'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    _ = require('lodash'),
    config = require('./config.json'),
    argv = require('./argv.js'),
    notify = require('gulp-notify');

gulp.task('lint', ['lint-back', 'lint-front', 'lint-spec']);

gulp.task('lint-back', function() {
  return gulp
    .src(config.selectors.backScripts)
    .pipe(jshint(_.extend({}, config.lint.common, config.lint.back)))
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'))
    .on('error', function(err) {
      if (argv.notify) {
        notify.onError(function(err) {
          return 'back-end lint error ' + err.message;
        })(err);
      }
    });
});

gulp.task('lint-front', function() {
  return gulp
    .src(config.selectors.frontScripts)
    .pipe(jshint(_.extend({}, config.lint.common, config.lint.front)))
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'))
    .on('error', function(err) {
      if (argv.notify) {
        notify.onError(function(err) {
          return 'front-end lint error ' + err.message;
        })(err);
      }
    });
});

gulp.task('lint-spec', function() {
  // TODO separate lint-back-spec and lint-front-spec
  // since the former has node: true and the latter not
  return gulp
    .src(config.selectors.specScripts)
    .pipe(jshint(_.extend({}, config.lint.common, config.lint.spec)))
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'))
    .on('error', function(err) {
      if (argv.notify) {
        notify.onError(function(err) {
          return 'front-end spec lint error ' + err.message;
        })(err);
      }
    });
});

// TODO lint-gulp