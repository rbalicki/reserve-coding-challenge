'use strict';

var gulp = require('gulp'),
    argv = require('./argv.js'),
    config = require('./config.json'),
    runSequence = require('run-sequence');

gulp.task('watch', function() {

  var tasks = [];

  (argv.watch && argv.watchImages) ? tasks.push('watch-images') : null;
  (argv.watch && argv.watchStylesheets) ? tasks.push('watch-stylesheets') : null;
  (argv.watch && argv.watchHtml) ? tasks.push('watch-templates') : null;
  (argv.watch && argv.watchHtml) ? tasks.push('watch-html') : null;
  (argv.watch && argv.watchFront) ? tasks.push('watch-scripts') : null;
  (argv.watch && argv.watchBack) ? tasks.push('watch-back') : null;
  (argv.watch && argv.watchSpec) ? tasks.push('watch-spec-front') : null;
  // watch gulp files
  // watch back spec

  return runSequence(tasks);
});

gulp.task('watch-images', function() {
  return gulp.watch(config.selectors.images,
    ['build-images']);
});

gulp.task('watch-stylesheets', function() {
  return gulp.watch(config.selectors.allStylesheets,
    ['build-stylesheets']);
});

gulp.task('watch-scripts', function() {

  var tasks = [];

  (argv.lint) ? tasks.push('lint-front') : null;
  (argv.build) ? tasks.push('build-scripts') : null;

  return gulp.watch(config.selectors.frontScripts,
    function () {
      runSequence.apply(this, tasks);
    });
});

gulp.task('watch-back', function() {

  var tasks = [];

  (argv.lint) ? tasks.push('lint-back') : null;
  (argv.liveReload) ? tasks.push('server-forever-restart') : null;

  return gulp.watch(config.selectors.backScripts,
    function () {
      runSequence.apply(this, tasks);
    });
});

gulp.task('watch-spec-front', function() {
  var tasks = [];

  (argv.lint) ? tasks.push('lint-spec') : null;

  return gulp.watch(config.selectors.frontSpecScripts,
    function () {
      runSequence.apply(this, tasks);
    });
});

gulp.task('watch-spec-back', function() {
  // not yet implemented
});

gulp.task('watch-html', function() {
  return gulp.watch(config.selectors.html,
    ['build-html']);
});

gulp.task('watch-templates', function() {
  return gulp.watch(config.selectors.templates,
    function () {
      runSequence('build-templates', 'build-html');
    });
});

// TODO watch gulp and at least lint it