'use strict';

var gulp = require('gulp'),
    pkg = require('../package.json'),
    config = require('./config.json'),
    fs = require('fs'),
    path = require('path'),
    rimraf = require('rimraf'),
    mainBowerFiles = require('main-bower-files'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    minifyCSS = require('gulp-minify-css'),
    jade = require('gulp-jade'),
    runSequence = require('run-sequence'),
    addSrc = require('gulp-add-src'),
    order = require('gulp-order'),
    reload = require('./live-reload.js'),
    indent = require('gulp-indent'),
    wrapper = require('gulp-wrapper');

gulp.task('build', function(cb) {
  runSequence(
    'build-clean',
    ['build-scripts', 'build-stylesheets', 'build-templates', 'build-images', 'build-html', 'build-fonts'],
    cb
  );
});

gulp.task('build-images', function() {
  return gulp
    .src(config.selectors.images)
    .pipe(gulp.dest(config.paths.build))
    .pipe(reload());
});

gulp.task('build-fonts', function() {
  return gulp
    .src(config.selectors.fonts)
    .pipe(gulp.dest(config.paths.build))
    .pipe(reload());
});

gulp.task('build-scripts', function() {
  return gulp
    .src(getMatchingBowerFiles(/.js$/))
    .pipe(addSrc(config.vendor.scripts))
    // "tag" the node_modules with different suffixes so that order
    // can place them in front
    .pipe(rename({ extname: '.vendor.js' }))
    .pipe(addSrc(config.selectors.frontScripts))
    // replace('.vendor.js', '.js') because of course, those aren't the real
    // extensions
    .pipe(indent(config.indent.options))
    .pipe(wrapper({
      header: function(file) {
        var fileName = file.path.replace('.vendor.js','.js');
        return '\n// Begin file: ' + fileName + '\n;(function() {\n';
      },
      footer: function(file) {
        var fileName = file.path.replace('.vendor.js','.js');
        return '\n})();\n// End file: ' + fileName + '\n';
      }
    }))
    .pipe(order(config.order))
    .pipe(concat(pkg.name + '.js'))
    .pipe(gulp.dest(config.paths.build))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest(config.paths.build))
    .pipe(reload());
});

gulp.task('build-stylesheets', function() {
  return gulp
    .src(getMatchingBowerFiles(/.less$/))
    .pipe(addSrc(config.selectors.stylesheets))
    .pipe(less())
    .pipe(addSrc(config.vendor.stylesheets))
    .pipe(concat(pkg.name + '.css'))
    .pipe(gulp.dest(config.paths.build))
    .pipe(minifyCSS())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest(config.paths.build))
    .pipe(reload());
});

gulp.task('build-html', ['build-templates'], function() {
  var templates = '';

  // if templates is not available (i.e. because there are no .template.jade files)
  // this task should not fail
  try {
    templates = fs.readFileSync(
      path.join(config.paths.build, pkg.name + '.templates.html')
    );
  } catch(e) {

  }

  return gulp
    .src(config.selectors.views)
    .pipe(jade({
      pretty: true,
      locals: {
        templates: templates,
        hrefs: {
          stylesheets: '/' + config.paths.public + '/' + pkg.name + '.min.css',
          scripts: '/' + config.paths.public + '/' + pkg.name + '.min.js'
        }
      }
    }))
    .pipe(gulp.dest(config.paths.build))
    .pipe(reload());

});

gulp.task('build-templates', function() {
  return gulp
    .src(config.selectors.templates)
    .pipe(jade({
      pretty: true,
      locals: {
        pkg: pkg,
        config: config
      }
    }))
    .pipe(wrapper(config.wrapper.templates))
    .pipe(concat(pkg.name + '.templates.html'))
    .pipe(gulp.dest(config.paths.build));
});

gulp.task('build-clean', function(cb) {
  rimraf(path.join(process.cwd(), config.paths.build), cb);
});

//////////////////

function getMatchingBowerFiles(re) {
  var bowerMain = mainBowerFiles(config.mainBowerFiles.options),
      matchingFiles = [];

  bowerMain.forEach(function(main) {
    if (typeof main === 'string') {
      ifMatchingPush(main);
    } else if (main.constructor === Array) {
      main.forEach(ifMatchingPush);
    }
  });

  return matchingFiles;
  
  /////////

  function ifMatchingPush(str) {
    if (re.exec(str)) {
      matchingFiles.push(str);
    }
  }
}