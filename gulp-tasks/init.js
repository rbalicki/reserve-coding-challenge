'use strict';

var gulp = require('gulp'),
    config = require('./config.json'),
    argv = require('yargs').argv,
    chalk = require('chalk'),
    fs = require('fs');

gulp.task('init-rename', function() {
  var pkg, bower, config;

  if (argv.name) {
    // NOTE: there appear to be some inconsistencies with how require
    // and writeFileSync handle paths. require('../PATH') and
    // fs.readFileSync('PATH') refer to the same file, presumably
    // because init.js is one level in

    pkg = require('../package.json');
    pkg.name = argv.name;
    fs.writeFileSync('package.json', JSON.stringify(pkg, null, 2));
    
    bower = require('../bower.json');
    bower.name = argv.name;
    fs.writeFileSync('bower.json', JSON.stringify(bower, null, 2));

    config = require('../app/back/config.json');
    config.DB.name = argv.name;
    fs.writeFileSync('app/back/config.json', JSON.stringify(config, null, 2));
  } else {
    console.log(chalk.red('init-rename error: --name parameter required'));
  }
});