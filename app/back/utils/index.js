'use strict';

var requireDir = require('require-dir'),
    _ = require('lodash');

module.exports = _.chain(require('require-dir')('.', {
    recurse: true
  }))
  .mapValues('index')
  .mapKeys(toCamelCase)
  .value();

function toCamelCase (value, fileName) {
  return fileName.replace(/[_-][a-z]/ig, function (s) {
    return s.substring(1).toUpperCase();
  });
}