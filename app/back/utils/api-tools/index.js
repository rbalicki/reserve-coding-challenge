'use strict';

var path = require('path'),
    config = require('app/config.json'),
    codes = {};

module.exports = {
  succeed: succeed,
  fail: fail,
  codes: codes,
  makeRoute: makeRoute,
  resolveOrReject: resolveOrReject
};

////////////

function succeed (req, res, payload) {
  res.status(200).send(payload);
}

function fail (req, res, errorCode, err) {
  res.status(errorCode).send(err);
}

function makeRoute (suffix) {
  return path.join(config.app.routes.api.route, suffix);
}

function resolveOrReject (deferred, err, doc) {
  if (err) {
    deferred.reject(err);
  } else {
    deferred.resolve(doc);
  }

  return deferred.promise;
}