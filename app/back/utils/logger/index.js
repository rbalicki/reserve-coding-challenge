'use strict';

var winston = require('winston'),
    argv = require('app/utils/argv'),
    config = require('app/config.json'),
    _ = require('lodash'),
    chalk = require('chalk'),
    path = require('path');

var logLevels = config.logger.logLevels;

module.exports = generateLogger;

///////

function generateLogger (dirname) {
  var logger = new (winston.Logger)({
        transports: [
          new (winston.transports.Console)({
            level: argv.consoleLogLevel
          })
        ]
      }),
      fileName = path.relative(
        path.join(process.cwd(), config.paths.backRoot),
        dirname
      ),
      toRet;

  return _.mapValues(logLevels, function (logLevel, name) {
    return function () {
      var chalkResult = chalk[logLevel.color].apply(chalk, arguments),
          args = [name].concat('\t' + fileName + '\t' + new Date() + '\t').concat(chalkResult);

      logger.log.apply(logger, args);
    };
  });
}