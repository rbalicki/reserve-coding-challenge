'use strict';

module.exports = createServer;

///////////

var path = require('path'),
    Q = require('q'),
    config = require('app/config.json'),
    appPromise = require('app/app'),
    http = require('http'),
    chalk = require('chalk');

function createServer () {
  return Q.all([
    appPromise()
  ]).spread(function (app) {
    var server = http.createServer(app),
        port = process.env.PORT || config.server.port;

    server.listen(port, function serverSuccess() {
      console.info(chalk.green('Server listening on port ' + port));
    });
  });

}