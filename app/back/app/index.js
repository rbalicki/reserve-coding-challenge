'use strict';

module.exports = createApp;

//////////

var express = require('express'),
    q = require('q');

function createApp() {
  var deferred = q.defer(),
      app = express();

  q.when(require('app/app/middleware')(app)).done(function() {
    q.when(require('app/app/routes')(app)).done(function() {
      deferred.resolve(app);
    });
  });

  return deferred.promise;
}