'use strict';

module.exports = bindFrontEndRoutes;

////////////

var q = require('q'),
    path = require('path'),
    config = require('app/config.json'),
    serveStatic = require('serve-static'),
    frontPath = path.join(process.cwd(), config.paths.front),
    angularPath = path.resolve(path.join(frontPath, 'index.html'));

function bindFrontEndRoutes (app) {
  app.use(config.app.routes.static.route,
    serveStatic(frontPath, config.app.routes.static.config)
  );

  loadAngularRoutes(app, config.app.routes.angularPaths);
}

function loadAngularRoutes (app, routes) {
  routes.forEach(function (route) {
    app.get(route, function (req, res) {
      res.sendFile(angularPath);
    });
  });
}