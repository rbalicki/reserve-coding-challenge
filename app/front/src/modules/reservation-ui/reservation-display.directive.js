'use strict';

angular.module('reserve.reservation-ui')

  .directive('resReservation', [

    function reservationDirective () {
      return {
        restrict: 'A',
        scope: {
          reservation: '=',
          restaurant: '='
        },
        controller: 'reserve.reservation-ui.ReservationdisplayController',
        controllerAs: 'ReservationdisplayController',
        templateUrl: 'modules/reservation-ui/reservation-display.template.html',
        bindToController: true,
      };
    }

  ])

  .controller('reserve.reservation-ui.ReservationdisplayController', [

    'lodash',
    '$q',
    '$interval',
    '$window',
    'reserve.reservations.ReservationConstant',

    function ReservationdisplayController (

      _,
      $q,
      $interval,
      $window,
      ReservationConstant

    ) {

      var vm = this,
          viewUpdateInterval;

      _.extend(vm, {
        getRemainingTimeString: getRemainingTimeString,
        states: ReservationConstant.states,
        isInErrorState: isInErrorState,
        submitReservation: submitReservation
      });

      viewUpdateInterval = $interval(function() {
        // update the view every second
      }, 1000);

      scheduleAlertPromise();

      //////////

      function getRemainingTimeString () {
        var remainingTime = vm.reservation.getRemainingTime();

        return remainingTime.minutes() + 'm ' + padZero(remainingTime.seconds()) + 's'; 
      }

      function padZero (num) {
        return num < 10 ? '0' + num : '' + num;
      }

      function isInErrorState () {
        // vm.parentReservationForm is defined in the view, by the form directive
        return Object.keys(vm.parentReservationForm.$error).length > 0;
      }

      function submitReservation () {
        if (!isInErrorState()) {
          return vm.reservation.savePendingData();
        } else {
          return $q.reject();
        }
      }

      function scheduleAlertPromise () {
        return vm.reservation.expirationPromise.then(function () {
          alert('Your reservation has expired');
          
          $window.location.reload();
        });
      }
    }
  ]);