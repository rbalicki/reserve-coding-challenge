'use strict';

angular.module('reserve.reservation-ui')

  .directive('resReservationInput', [

    function reservationInput () {
      return {
        restrict: 'A',
        scope: {
          model: '=',
          errorMessage: '=',
          placeholder: '=',
          reMatch: '='
        },
        controller: 'reserve.reservation-ui.ReservationinputController',
        controllerAs: 'ReservationinputController',
        bindToController: true,
        templateUrl: 'modules/reservation-ui/reservation-input/reservation-input.template.html'
      };
    }

  ])

  .controller('reserve.reservation-ui.ReservationinputController', [

    'lodash',

    function ReservationinputController (

      _

    ) {

      var vm = this;

      _.extend(vm, {
        isInErrorState: isInErrorState,
        isTouched: isTouched,
        regex: new RegExp(vm.reMatch)
      });

      //////////////

      // vm.reservationInputForm is defined in the view, by the form directive

      function isInErrorState () {
        // note this is not safe if key does not exist... IRL this would be cleaned up
        return Object.keys(vm.reservationInputForm.input.$error).length > 0;
      }

      function isTouched () {
        return vm.reservationInputForm.input.$touched;
      }
    }

  ]);
