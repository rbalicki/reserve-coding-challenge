'use strict';

angular.module('reserve.reservations')

  .factory('reserve.reservations.ReservationsService', [

    'moment',
    '$q',
    'reserve.reservations.Reservation',
    'reserve.reservations.ReservationConstant',

    function ReservationsService (

      moment,
      $q,
      Reservation,
      ReservationConstant

    ) {

      // As with RestaurantsService, all of this would be done through
      // an API instead of being mocked locally.

      var TIME_HELD = 5 * 60,
          mockPayload = {
            time: moment('Sep 30 2016 22', 'MMM DD YYYY HH').toISOString(),
            heldUntil: moment().add(TIME_HELD, 's').toISOString(),
            numberOfGuests: 4,
            resturantId: 'abcdef',
            state: ReservationConstant.states.UNCONFIRMED
          };


      return {
        getReservation: getReservation
      };

      /////////////

      function getReservation (reservationId) {
        // Pretend that there is a previous screen where we checked the availability
        // of a reservation at a given time

        // SHORTCUT: destructively modify mockPaylod instead of copying it... doesn't matter
        mockPayload.id = reservationId;

        return $q.when(new Reservation(mockPayload));
      }

    }

  ]);