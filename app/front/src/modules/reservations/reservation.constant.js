'use strict';

angular.module('reserve.reservations')

  .constant('reserve.reservations.ReservationConstant', {

    states: {
      LOADING: 'loading',
      UNCONFIRMED: 'unconfirmed',
      CONFIRMED: 'confirmed',
      EXPIRED: 'expired'
    }

  });