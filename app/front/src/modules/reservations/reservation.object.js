'use strict';

angular.module('reserve.reservations')

  .factory('reserve.reservations.Reservation', [

    'lodash',
    'moment',
    '$timeout',
    '$q',
    'reserve.reservations.ReservationConstant',

    function (

      _,
      moment,
      $timeout,
      $q,
      ReservationConstant

    ) {

      return Reservation;

      ///////////

      function Reservation (hash) {
        var privateData = _.extend({}, hash);


        _.extend(this, {
          savePendingData: savePendingData.bind(this, privateData),
          getState: getState.bind(this, privateData),
          getNumberOfGuests: getNumberOfGuests.bind(this, privateData),
          getReservationTime: getReservationTime.bind(this, privateData),
          getRemainingTime: getRemainingTime.bind(this, privateData),
          getFirstName: getFirstName.bind(this, privateData),
          getSpecialRequests: getSpecialRequests.bind(this, privateData)
        });

        if (this.getState() === ReservationConstant.states.UNCONFIRMED) {
          initializeUnconfirmedReservation.call(this, privateData);
        }
      }

      function savePendingData (privateData) {
        // jshint validthis: true
        var reservation = this;

        if (this.getState() === ReservationConstant.states.UNCONFIRMED) {
          privateData.state = ReservationConstant.states.LOADING;

          return $timeout(function() {
            // In practice, we'd do this in a more robust way (white-list properties, for example)
            _.extend(privateData, reservation.pendingData || {});
            privateData.state = ReservationConstant.states.CONFIRMED;

          }, 3000);
        } else {
          return $q.reject(new Error('Reservation has expired'));
        }
      }

      function getState (privateData) {
        return privateData.state;
      }

      function getNumberOfGuests (privateData) {
        return privateData.numberOfGuests;
      }

      function getReservationTime (privateData) {
        return moment(privateData.time);
      }

      function getSpecialRequests (privateData) {
        return privateData.specialRequests;
      }

      function getFirstName (privateData) {
        return privateData.firstName;
      }

      function getRemainingTime (privateData) {
        // jshint validthis: true
        if (this.getState() === ReservationConstant.states.UNCONFIRMED) {
          return moment.duration(privateData.heldUntil.diff(moment()));
        } else {
          // maybe throw an error instead of returning a duration of time 0?
          return moment.duration(moment().diff(moment()));
        }
      }

      function initializeUnconfirmedReservation (privateData) {
        // jshint validthis: true
        var reservation = this;

        privateData.heldUntil = moment(privateData.heldUntil);
        reservation.pendingData = {};
        reservation.expirationPromise = $timeout(function reservationIsExpiring () {
          if (reservation.getState() === ReservationConstant.states.UNCONFIRMED) {
            privateData.state = ReservationConstant.states.EXPIRED;
          } else {
            // if the reservation is not in the UNCONFIRMED state, the promise never resolves
            return $q.defer().promise;
          }
        },  this.getRemainingTime().asMilliseconds() + 200);
        // +200 ms to be safe... in practice I'd write unit tests to be sure instead
      }

    }

  ]);