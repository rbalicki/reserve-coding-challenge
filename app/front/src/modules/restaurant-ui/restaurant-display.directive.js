'use strict';

angular.module('reserve.restaurant-ui')

  .directive('resRestaurant', [

    function restaurantDirective () {
      return {
        restrict: 'A',
        scope: {
          restaurant: '='
        },

        // needed for controllerAs and bindToController
        controller: function () {},
        controllerAs: 'RestaurantdisplayController',
        templateUrl: 'modules/restaurant-ui/restaurant-display.template.html',
        bindToController: true
      };
    }

  ]);