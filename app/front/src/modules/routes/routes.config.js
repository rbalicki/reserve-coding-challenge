'use strict';

angular.module('reserve.routes')
  
  .config([

    '$locationProvider',
    '$urlRouterProvider',
    '$urlMatcherFactoryProvider',

    function(

      $locationProvider,
      $urlRouterProvider,
      $urlMatcherFactoryProvider

    ) {

      $locationProvider.html5Mode(true);

      $urlRouterProvider.otherwise('/');

      $urlMatcherFactoryProvider.strictMode(false);

    }

  ]);