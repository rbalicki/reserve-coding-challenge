'use strict';

angular.module('reserve.routes.confirm-reservation', [
  'ui.router',
  'reserve.globals',
  'reserve.reservations',
  'reserve.reservation-ui',
  'reserve.restaurants',
  'reserve.restaurant-ui'
]);