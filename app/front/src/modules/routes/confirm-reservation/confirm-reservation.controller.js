'use strict';

angular.module('reserve.routes.confirm-reservation')

  .controller('reserve.routes.confirm-reservation.ConfirmreservationController', [

    'lodash',
    'restaurant',
    'reservation',

    function ConfirmreservationController (

      _,
      restaurant,
      reservation

    ) {

      var vm = this;

      _.extend(vm, {
        restaurant: restaurant,
        reservation: reservation
      });

    }

  ]);