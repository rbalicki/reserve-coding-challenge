'use strict';

angular.module('reserve.routes.confirm-reservation')
  
  .config([

    '$stateProvider',

    function(

      $stateProvider

    ) {

      $stateProvider.state('confirm-reservation', {
        url: '/confirm-reservation',
        views: {
          main: {
            template: '<ui-view />',
            controller: [
              '$state',

              function(

                $state

              ) {
                // In practice, we wouldn't redirect to someId, but would go to
                // a previous page

                if (!(/^confirm-reservation.detail$/.exec($state.current.name))) {
                  $state.go('confirm-reservation.detail', {
                    reservationId: 'my-reservation-id'
                  });
                }
              }
            ]
          }
        }
      });

      // ASSUME: there was a previous screen in which a post was made to /reservations
      // that returned a reservationId for an unconfirmed (partial) reservation.
      $stateProvider.state('confirm-reservation.detail', {
        url: '/:reservationId',
        templateUrl: 'modules/routes/confirm-reservation/confirm-reservation.template.html',
        controller: 'reserve.routes.confirm-reservation.ConfirmreservationController',
        controllerAs: 'ConfirmreservationController',
        resolve: {
          restaurant: [

            '$stateParams',
            'reserve.reservations.ReservationsService',
            'reserve.restaurants.RestaurantsService',

            function getRestaurant (

              $stateParams,
              ReservationsService,
              RestaurantsService

            ) {

              return ReservationsService.getReservation($stateParams.reservationId)
                .then(function getReservationSucceeded (reservation) {
                  return RestaurantsService.getRestaurant(reservation.restaurantId);
                });

            }
          ],
          reservation: [
            '$stateParams',
            'reserve.reservations.ReservationsService',

            function getInitialReservation (

              $stateParams,
              ReservationsService

            ) {

              return ReservationsService.getReservation($stateParams.reservationId);

            }
          ]
        }
      });

    }

  ]);