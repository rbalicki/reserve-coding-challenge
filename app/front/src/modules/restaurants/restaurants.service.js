'use strict';

angular.module('reserve.restaurants')

  .factory('reserve.restaurants.RestaurantsService', [

    '$q',

    function RestaurantsService (

      $q

    ) {

      var mockPayload = {
        name: 'Café Reserve',

        // In practice, we would get these from S3 or something
        backgroundImageUrl: 'modules/restaurant-ui/images/restaurant-cropped.jpg',

        // in reality a complex hash
        addressString: '1 Union Square, New York NY 10003\n(917) 555-3838'
      };

      return {
        getRestaurant: getRestaurant
      };

      /////////////

      function getRestaurant (id) {
        // In practice, we would make an HTTP request for the restaurant
        // and cache the results.

        // NOTE: I'm aware I'm destructively modifying mockPayload for convenience
        mockPayload.id = id;

        return $q.when(mockPayload);
      }

    }

  ]);