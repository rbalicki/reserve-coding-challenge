'use strict';

var requireDir = require('require-dir'),
    dir = requireDir('./gulp-tasks'),
    gulp = require('gulp'),
    argv = require('./gulp-tasks/argv.js'),
    config = require('./gulp-tasks/config.json'),
    runSequence = require('run-sequence');

gulp.task('default', function() {
  var tasks = [];

  (argv.lint) ? tasks.push('lint') : null;
  (argv.build) ? tasks.push('build') : null;
  (argv.watch) ? tasks.push('watch') : null;
  (argv.server) ? tasks.push('server-forever') : null;
  (argv.liveReload) ? tasks.push('live-reload-start') : null;

  runSequence.apply(this, tasks);

});